const express = require('express');
const app = express();
const fs = require('fs');
const bodyParser = require('body-parser');

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) =>{
    res.send('Welcome');
});

app.get('/employees', (req, res) =>{
    fs.readFile(__dirname + '/data/employee.json', 'utf-8', (err , data) => {
        res.send(data);
    })
});

app.post('/add', (req, res) => {
    const emp = req.body;
    const data = loadData(__dirname + '/data/employee.json');
    if(!data){
        res.send('error');
    }
    data.push(emp);
    storeData(data, __dirname+'/data/employee.json');
    res.json(data);
});


const loadData = (path) => {
    try {
      return JSON.parse(fs.readFileSync(path, 'utf8'));
    } catch (err) {
      console.error(err);
      return false;
    }
  }

  const storeData = (data, path) => {
    try {
      fs.writeFileSync(path, JSON.stringify(data))
      return true;
    } catch (err) {
      console.error(err)
        return false;
    }
  }

app.listen(8000, () => {
    console.log('Appilcation is running on port 8000');
});


